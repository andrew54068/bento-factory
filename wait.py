from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import wait

waitTimeout = 5


def iframes(driver):
    for i in range(0, 2):
        try:
            result = WebDriverWait(driver, waitTimeout).until(
                lambda x: x.find_elements(By.TAG_NAME, "iframe")
            )
            if result is None:
                raise TimeoutException()
            else:
                return result
        except TimeoutException:
            pass
    raise TimeoutException()


def element_clickable(driver, element):
    for i in range(0, 2):
        try:
            result = WebDriverWait(driver, waitTimeout).until(
                EC.element_to_be_clickable(element)
            )
            if result is None:
                raise TimeoutException()
            else:
                return result
        except TimeoutException:
            pass
    raise TimeoutException()


def display_xpath(driver, path):
    for i in range(0, 2):
        try:
            result = WebDriverWait(driver, waitTimeout).until(
                lambda x: x.find_element(By.XPATH, path)
            )
            if result is None:
                raise TimeoutException()
            else:
                return result
        except TimeoutException:
            pass
    raise TimeoutException()


def clickable_xpath(driver, path):
    for i in range(0, 2):
        try:
            result = WebDriverWait(driver, waitTimeout).until(
                EC.element_to_be_clickable((By.XPATH, path))
            )
            if result is None:
                raise TimeoutException()
            else:
                return result
        except TimeoutException:
            pass
    raise TimeoutException()


def display(driver, css_selector, timeout=waitTimeout):
    for i in range(0, 2):
        try:
            result = WebDriverWait(driver, timeout).until(
                lambda x: x.find_element(By.CSS_SELECTOR, css_selector)
            )
            if result is None:
                raise TimeoutException()
            else:
                return result
        except TimeoutException:
            pass
    raise TimeoutException()

def display_elements(driver, css_selector, timeout=waitTimeout):
    for i in range(0, 2):
        try:
            result = WebDriverWait(driver, timeout).until(
                lambda x: x.find_elements(By.CSS_SELECTOR, css_selector)
            )
            if result is None:
                raise TimeoutException()
            else:
                return result
        except TimeoutException:
            pass
    raise TimeoutException()


def clickable(driver, css_selector):
    for i in range(0, 2):
        try:
            result = WebDriverWait(driver, waitTimeout).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, css_selector))
            )
            if result is None:
                raise TimeoutException()
            else:
                return result
        except TimeoutException:
            pass
    raise TimeoutException()


def element_by_text(firefox, text, hierarchy):
    connectText = wait.display_xpath(firefox, f"//*[text()='{text}']")
    element = connectText.find_element(By.XPATH, hierarchy)
    return element

def button_by_text(firefox, text, hierarchy):
    connectText = wait.display_xpath(firefox, f"//*[text()='{text}']")
    button = connectText.find_element(By.XPATH, hierarchy)
    button = wait.element_clickable(firefox, button)
    return button

def button_by_text_for_mantine(firefox, text):
    connectText = wait.display_xpath(firefox, f"//*[text()='{text}']")
    button = connectText.find_element(By.XPATH, "./../..")
    button = wait.element_clickable(firefox, button)
    return button

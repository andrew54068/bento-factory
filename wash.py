import time
import os
from concurrent.futures import ThreadPoolExecutor, as_completed

from dotenv import load_dotenv
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException, NoSuchWindowException

import blocto
import gmail
import wait
import custom_error

load_dotenv(override=True)


def deal_with_case(firefox, email, creds_file, func, original_func, retry=False):

    ctaBtn = wait.display(firefox, ".css-1h6lxi")
    ctaBtnText = ctaBtn.find_element(By.CSS_SELECTOR, ".css-5r969c")
    if ctaBtnText.text == "Batcha!":
        pass
    if ctaBtnText.text == "Connect Wallet":
        ctaBtn = wait.clickable(firefox, ".css-1h6lxi")
        try:
            ctaBtn.click()
            time.sleep(1)
            bloctoOption = wait.element_by_text(firefox, "Blocto", ".")
            bloctoOption.click()
            blocto.connect_wallet(firefox, email, creds_file)
        except TimeoutException:
            pass
    if ctaBtnText.text == "Switch Network":
        time.sleep(1)
        ctaBtn.click()
        blocto.switch_chain(firefox)
        time.sleep(2)

    time.sleep(1)
    func(firefox, email, creds_file, retry)

    batchaBtn = wait.clickable(firefox, ".css-1h6lxi")
    batchaBtn.click()

    try:
        blocto.send_tx(firefox, email)
    except (
        custom_error.NeedRefreshError,
        NoSuchWindowException,
        TimeoutException,
    ) as e:
        if retry:
            raise custom_error.ContinueError("Try another case.")
        else:
            return original_func(firefox, email, creds_file, retry=True)
    except custom_error.ContinueError:
        pass

    try:
        return save_transaction(firefox)
    except TimeoutException:
        try:
            wait.display_xpath(firefox, "//*[text()='Something Went Wrong']")
            print("💥 Something Went Wrong")
            raise custom_error.NeedRefreshError()
        except TimeoutException:
            try:
                return save_transaction(firefox)
            except TimeoutException:
                return custom_error.ContinueError("Try another case.")


def save_transaction(firefox):
    time.sleep(1)
    checkTxBtn = wait.clickable(firefox, ".css-66aow7")
    link = checkTxBtn.get_attribute("href")
    txHash = link.split("/")[-1]
    print(txHash)

    closeBtn = wait.clickable(firefox, ".css-1ik4h6n")
    closeBtn.click()

    with open("transactions.txt", "a") as f:
        f.write("in yearn: ")
        f.write(txHash)
        f.write("\n")
    return txHash


def bento_batch_deposit(firefox, email, creds_file, retry=False):
    try:
        url = f"https://bentobatch.com/case/yearn_v3_usdt_a?utm_source=0xAf5F6A8A3102734E19c577cd625835eaa077765e&utm_medium=batch_success&utm_campaign=referral"
        firefox.get(url)

        time.sleep(3)
        return deal_with_case(
            firefox, email, creds_file, deposit_content, bento_batch_deposit, retry
        )
    except TimeoutException:
        if retry:
            raise custom_error.ContinueError("Try another case.")
        return bento_batch_deposit(firefox, email, creds_file, retry=True)


def deposit_content(firefox, email, creds_file, retry):
    balanceContainer = wait.display(firefox, ".css-1572pvc")
    balance = balanceContainer.text.split()[1]

    if balance == "-" or float(balance) == 0:
        if retry:
            raise custom_error.ContinueError("Try another case.")
        firefox.refresh()
        time.sleep(1)
        hash = bento_batch_deposit(firefox, email, creds_file, retry=True)
        return hash

    amountInput = wait.display(firefox, ".css-1szydlo")
    amountInput.send_keys(balance)

    maxBtn = wait.clickable(firefox, ".css-1v71pvf")
    maxBtn.click()


def bento_batch_withdrawal(firefox, email, creds_file, retry=False):
    try:
        url = f"https://bentobatch.com/case/yearn_withdrawal?utm_source=0xAf5F6A8A3102734E19c577cd625835eaa077765e&utm_medium=batch_success&utm_campaign=referral"
        firefox.get(url)

        return deal_with_case(
            firefox,
            email,
            creds_file,
            withdrawal_content,
            bento_batch_withdrawal,
            retry,
        )
    except (TimeoutException, custom_error.ContinueError):
        if retry:
            return ""
        return bento_batch_withdrawal(firefox, email, creds_file, retry=True)


def withdrawal_content(firefox, email, creds_file, retry):
    balanceContainers = wait.display_elements(firefox, ".css-1572pvc")
    balance = balanceContainers[2].text.split()[1]

    if balance == "-" or float(balance) == 0:
        if retry:
            raise custom_error.ContinueError("Try another case.")
        firefox.refresh()
        time.sleep(1)
        hash = bento_batch_withdrawal(firefox, email, creds_file, retry=True)
        return hash

    amountInputs = wait.display_elements(firefox, ".css-1szydlo")
    amountInputs[2].send_keys(balance)

    maxBtns = wait.display_elements(firefox, ".css-1v71pvf")
    maxBtn = maxBtns[2]
    wait.element_clickable(firefox, maxBtn)
    maxBtn.click()


def kitchen_send(firefox, email, creds_file, retry=False):
    url = f"http://localhost:3000/yearn_withdrawal"
    firefox.get(url)

    try:
        wait.button_by_text_for_mantine(firefox, "Disconnect")
    except TimeoutException:
        connectBtn = wait.button_by_text_for_mantine(firefox, "Connect")
        connectBtn.click()

        bloctoBtn = wait.button_by_text_for_mantine(firefox, "Blocto")
        bloctoBtn.click()
        blocto.connect_wallet(firefox, email, creds_file)

        switchToText = wait.display_xpath(
            firefox, "//*[text()[contains(.,'Swtich to')]]"
        )
        button = switchToText.find_element(By.XPATH, "./../..")
        button = wait.element_clickable(firefox, button)
        button.click()
        blocto.switch_chain(firefox)

        time.sleep(2)

    bloctoBtn = wait.button_by_text_for_mantine(firefox, "Max")
    balanceInput = wait.display(firefox, ".m-8fb7ebe7")
    placeholder = balanceInput.get_attribute("placeholder")

    if placeholder == "0":
        if retry:
            raise custom_error.ContinueError("Try another case.")
        firefox.refresh()
        return kitchen_send(firefox, email, creds_file, True)

    bloctoBtn = wait.button_by_text_for_mantine(firefox, "Max")
    bloctoBtn.click()

    genBtn = wait.button_by_text_for_mantine(firefox, "Gen Batch")
    genBtn.click()

    batchaBtn = wait.button_by_text_for_mantine(firefox, "Batcha!")
    batchaBtn.click()

    time.sleep(3)

    try:
        blocto.send_tx(firefox, email)
    except (
        custom_error.NeedRefreshError,
        NoSuchWindowException,
        TimeoutException,
    ) as e:
        if retry:
            raise custom_error.ContinueError("Try another case.")
        return kitchen_send(firefox, email, creds_file, True)
    except custom_error.ContinueError:
        pass

    switchToText = wait.display_xpath(firefox, "//*[text()[contains(.,'txHash: ')]]")
    try:
        txHash = switchToText.text.split()[-1]
    except:
        print(switchToText.text)
        txHash = switchToText.text
    print(txHash)
    with open("transactions.txt", "a") as f:
        f.write("out yearn: ")
        f.write(txHash)
        f.write("\n")
    return txHash


def change_account(driver, original_email, creds_file):
    next_email = blocto.next_email(original_email)
    to_address = blocto.register_account(driver, next_email)
    transfer_asset(driver, original_email, to_address)
    blocto.connect_wallet(driver, next_email, creds_file)
    main(driver, next_email)


def transfer_asset(driver, email, to_address, creds_file):
    blocto.connect_wallet(driver, email, creds_file)
    # TODO:
    pass


def open_tab(driver):
    driver.find_element(By.TAG_NAME, "body").send_keys(Keys.COMMAND + "t")


def switch_tab(driver):
    driver.find_element(By.TAG_NAME, "body").send_keys(Keys.CONTROL + Keys.TAB)


def main(driver, email, creds_file, inTxs=[]):
    _inTxs = inTxs
    try:
        while len(_inTxs) < 20:
            print(f'_inTxs: {len(_inTxs)}')
            try:
                inTx = bento_batch_deposit(driver, email, creds_file)
                print(f'inTx: {inTx}')
                _inTxs.append(inTx)
            except custom_error.ContinueError:
                pass

            try:
                if len(_inTxs) < 19:
                    outTx = bento_batch_withdrawal(driver, email, creds_file)
                    # outTx = kitchen_send(driver, email, creds_file)
                else:
                    driver.close()
                # outTx = bento_batch_withdrawal(driver, email, creds_file)
            except custom_error.ContinueError:
                pass
    except custom_error.StopError:
        return driver.close()
    except Exception as e:
        print(e)
        main(driver, email, creds_file, _inTxs)


def detect_gmail_token_files(directory):
    # List all files in the given directory
    files_in_directory = os.listdir(directory)

    # Filter files that contain 'gmail_token' in their name
    gmail_token_files = [f for f in files_in_directory if "gmail_token" in f]
    return gmail_token_files


if __name__ == "__main__":
    emailsText = os.getenv("EMAILS")
    credsFilesText = os.getenv("CREDS_FILES")
    emails = emailsText.split(",")
    credsFiles = credsFilesText.split(",")
    print(emails)

    root_directory = os.getcwd()
    gmail_token_files = detect_gmail_token_files(root_directory)

    if (gmail_token_files is None) or (len(gmail_token_files) == 0):
        gmail.generate_gmail_token()

    if len(emails) != len(credsFiles):
        print(
            "emails amount is not equal to credsFiles provided, please correct it from .env file."
        )
        exit(1)

    future_list = []
    with ThreadPoolExecutor(max_workers=len(gmail_token_files)) as executor:
        for index, token_file in enumerate(gmail_token_files):
            print(f'index: {index}, emails: {emails}')
            try:
                future = executor.submit(
                    main, webdriver.Firefox(), emails[index], token_file
                )
                future_list.append(future)
            except Exception as e:
                print(e)
                continue

    for future in future_list:
        try:
            print("future")
            print(future.result())
        except Exception as e:
            print(e)

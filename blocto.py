import time
import os
import re
import pyperclip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchWindowException

import custom_error
import wait
import gmail
import wash


def tpye_otp_code(driver, code):
    for i in range(0, 6):
        id = f"pin-input-:r0:-{i}"
        input = driver.find_element(By.ID, id)
        input.send_keys(code[i])


def connect_wallet(driver, email, creds_file):
    time.sleep(2)
    iframes = wait.iframes(driver)
    driver.switch_to.frame(iframes[0])
    try:
        # already logged in before
        otpConfirmBtn = wait.display_xpath(
            driver, "//*[text()='核准' or text()='Approve']"
        )
        if otpConfirmBtn is None:
            raise TimeoutException()
        parent = otpConfirmBtn.find_element(By.XPATH, "./..")
        wait.element_clickable(driver, parent)
        parent.click()
        driver.switch_to.default_content()
        return
    except TimeoutException:
        print("log in first time")
        __enter_email_otp_code(driver, email, creds_file)


def __enter_email_otp_code(driver, email, creds_file):
    emailInput = wait.display(driver, ".css-g5jozo")
    emailInput.send_keys(email)

    loginButton = wait.clickable(driver, ".css-1byb74s")
    loginButton.click()

    otp = None
    while otp is None:
        try:
            time.sleep(5)
            otp = gmail.get_otp_code(creds_file)
            print(otp)
        except gmail.GmailError:
            continue
        break

    tpye_otp_code(driver, otp)

    address_cell = wait.display_elements(driver, ".css-1o8n9tp", 10)[1]
    address_cell.click()

    address = pyperclip.paste()

    otpConfirmBtn = wait.display_xpath(driver, "//*[text()='核准' or text()='Approve']")
    parent = otpConfirmBtn.find_element(By.XPATH, "./..")
    wait.element_clickable(driver, parent)
    parent.click()

    driver.switch_to.default_content()
    return address


def switch_chain(driver):
    time.sleep(2)
    iframes = wait.iframes(driver)
    driver.switch_to.frame(iframes[0])
    confirmSwitchBtn = wait.clickable(driver, ".css-bvcbfj")
    confirmSwitchBtn.click()
    driver.switch_to.default_content()
    time.sleep(2)


def send_tx(driver, email):
    time.sleep(2)
    iframes = wait.iframes(driver)
    driver.switch_to.frame(iframes[0])

    try:
        time.sleep(2)
        click_approve(driver)
    except TimeoutException:
        try:
            tryBtn = wait.display(driver, ".css-1qx65r7")
            tryBtn.click()
            click_approve(driver)
            return
        except TimeoutException:
            pass

        try:
            wait.display_xpath(
                driver,
                "//*[text()='交易互動失敗' or contains(text(), 'Failed') or contains(text(), 'Unexpected Error') or contains(text(), 'asset is not confirmed')]",
            )
            raise custom_error.NeedRefreshError("failed to send tx")
        except TimeoutException:
            click_approve(driver)


def click_approve(driver):
    print("try to find approveBtn")
    approveBtn = wait.display_xpath(
        driver,
        "//*[text()='同意' or contains(text(), '同意') or contains(text(), 'Approve')]",
    )
    # approveBtn = wait.display(driver, ".css-1byb74s")
    print("found approveBtn")
    # approveBtn = wait.clickable(driver, ".css-1byb74s")
    bloctoPoint = driver.find_elements(By.CSS_SELECTOR, ".css-m1036o")[1]
    print("found bloctoPoint")
    if int(bloctoPoint.text.replace(",", "")) <= 100:
        # save some point for the future usage.
        # change to another account
        print("🙂 save some point for the future")
        driver.switch_to.default_content()
        # wash.change_account(driver, email)
        raise custom_error.StopError("save some point for the future")
    else:
        print(f"Point still enough {bloctoPoint.text}")

    approveBtn.click()
    try:
        okBtn = wait.clickable(driver, ".css-bvcbfj")
        okBtn.click()
        driver.switch_to.default_content()
        time.sleep(2)
    except NoSuchWindowException:
        raise custom_error.ContinueError("Shouldn't matter.")


def change_account():
    exit(1)


def register_account(driver, email, creds_file):
    time.sleep(2)
    iframes = wait.iframes(driver)
    driver.switch_to.frame(iframes[0])

    print(f"sign up: {email}")
    address = __enter_email_otp_code(driver, email, creds_file)

    return address


def modify_email(original_email, index):

    # Split the email address into username and domain
    username, domain = original_email.split("@")

    if "+" in username:
        # If the '+' sign is present, replace the number with the desired number 'n'
        username, original_index = username.split("+")

    new_username = username + "+" + str(index)

    # Return the modified email address
    return new_username + "@" + domain


def next_email(email):

    # Split the email address into username and domain
    username, domain = email.split("@")

    if "+" in username:
        # If the '+' sign is present, replace the number with the desired number 'n'
        username, original_index = username.split("+")
        try:
            current_index = int(original_index)
            return username + str(int(current_index) + 1) + "@" + domain
        except ValueError:
            print(f"original_index is {original_index}, but it should be a number.")
            exit(1)

    new_username = username + "+" + str(1)

    return new_username + "@" + domain


def register_multiple_account(original_email, start_index, end_index, creds_file):

    for index in range(start_index, end_index):
        driver = webdriver.Firefox()
        url = "https://bentobatch.com/case/ether_fi_arbitrum_bridge"
        driver.get(url)
        connectBtn = wait.display(driver, ".css-172ut88", 5)
        connectBtn.click()

        new_email = modify_email(original_email, index)
        address = register_account(driver, new_email, creds_file)
        print(f"index: {index}, email: {new_email}, address: {address}\n")
        with open("accounts.txt", "a") as f:
            f.write(f"email: {new_email},")
            f.write(f"address: {address}")
            f.write("\n")


if __name__ == "__main__":
    email = os.getenv("EMAIL")
    print(email)
    start_index = 3
    end_index = 5
    creds_file = os.getenv("CREDS_FILE")
    print(creds_file)
    register_multiple_account(email, start_index, end_index, creds_file)

from simplegmail import Gmail
from simplegmail.query import construct_query
import re

# For even more control use queries:
# Messages that are either:
#   newer than 2 days old, unread, labeled "Finance" or both "Homework" and "CS"
#     or
#   newer than 1 month old, unread, labeled "Top Secret", but not starred.

# Construct our two queries separately
query_params_1 = {
    "sender": "system@blocto.app",
    "newer_than": (1, "day"),
    "label": ["INBOX", "UNREAD"],
    "subject": ["Blocto Verification Code", "Blocto 一次性驗證碼通知"],
}

query_params_2 = {
    "sender": "system@blocto.app",
    "newer_than": (1, "day"),
    "label": ["INBOX", "UNREAD"],
    "subject": ["Blocto Verification Code", "Blocto 一次性驗證碼通知"],
}


class GmailError(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


def generate_gmail_token():
    Gmail()


def get_otp_code(creds_file: str = "gmail_token.json"):
    gmail = Gmail(creds_file=creds_file)

    # construct_query() will create both query strings and "or" them together.
    messages = gmail.get_messages(query=construct_query(query_params_1, query_params_2))
    if len(messages) > 0:
        foundMessage = messages[0]
        input_string = foundMessage.snippet
        pattern = r"\((.*?)\)"

        # Using re.findall to extract content between parentheses
        matches = re.findall(pattern, input_string)

        # Output the result
        if matches:
            foundMessage.remove_label("UNREAD")
            foundMessage.move_from_inbox(["TRASH"])
            return matches[0]
        else:
            raise GmailError("No match found.")
    else:
        raise GmailError("code not found.")


if __name__ == "__main__":
    get_otp_code()

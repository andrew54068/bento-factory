class NeedRefreshError(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class ContinueError(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message
    
class StopError(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message
